﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryClient.Models
{
    public class Copy : Book
    {
        public int ID { get; set; }
        public string CopyISBN { get; set; }
        public int CopyLibraryID { get; set; }

        public Copy()
        {

        }
    }
}
