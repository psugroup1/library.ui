﻿namespace LibraryClient.Models.Enums
{
    public enum PersonType
    {
        Member = 0,
        Professor = 1,
    }
}