﻿namespace LibraryClient.Models.Enums
{
    public enum SubjectArea
    {
        ComputerScience = 0,
        Economics = 1,
        Psychology = 2,
        Law = 3
    }
}
