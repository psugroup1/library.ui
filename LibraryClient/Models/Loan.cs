﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryClient.Models
{
    public class Loan
    {
        public DateTime DateCreated { get; set; }
        public DateTime DateReturned { get; set; }
        public int PersonCardID { get; set; }
        public int CopyId { get; set; }
        public int LoanId { get; set; }


        public Loan()
        {

        }
    }
}
