﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using LibraryClient.Integration;
using LibraryClient.Models;
using LibraryClient.Models.Enums;
using LibraryClient.UI.Annotations;

namespace LibraryClient.UI
{
    /// <summary>
    /// Interaction logic for SearchForBookPage.xaml
    /// </summary>
    public partial class SearchForBookPage : Page, INotifyPropertyChanged
    {
        //Variables
        private Book _selectedBook;
        private int _bookAvailability;
        ObservableCollection<Book> mBooks = new ObservableCollection<Book>();
        LoanGateway lgw = new LoanGateway();

        //Constructor
        public SearchForBookPage()
        {
            InitializeComponent();
            DataContext = this;
        }

        //Methods for retrieving variables
        public Book SelectedBook
        {
            get { return _selectedBook; }
            set
            {
                _selectedBook = value;
                if (value == null)
                {
                    BookAvailability = 0;
                }
                else
                {
                    //Nok ikke lige den bedste måde at få dette check ind på...
                    BookAvailability = lgw.CheckBookAvailability(SelectedBook.ISBN);
                }
                OnPropertyChanged();
            }
        }
        public ObservableCollection<Book> Books
        {
            get
            {
                return mBooks;
            }
        }

        public int BookAvailability
        {
            get { return _bookAvailability; }
            set
            {
                _bookAvailability = value;
                OnPropertyChanged();
            }
        }

        //Methods
        private void Button_Click_SearchTitle(object sender, RoutedEventArgs e)
        {
            StatusTextblock.Text = "";
            var amountOfBooks = 0;
            mBooks.Clear();
            var oc = new ObservableCollection<Book>();
            var result = lgw.SearchBooksTitle(TitleTextbox.Text);
            //Dispatcher.BeginInvoke((Action)(() =>
            //{
                result.ForEach(x => mBooks.Add(x));
            //}));
            amountOfBooks = result.Count();
            StatusTextblock.Text = $"Found {amountOfBooks} books by searching for title: \"{TitleTextbox.Text}\"";
            TitleTextbox.Text = "Title";
            TitleTextbox.GotFocus += TextBox_GotFocus;

        }
        private void Button_Click_SearchAuthor(object sender, RoutedEventArgs e)
        {
            StatusTextblock.Text = "";
            var amountOfBooks = 0;
            mBooks.Clear();
            var oc = new ObservableCollection<Book>();
            var result = lgw.SearchBooksAuthor(AuthorTextbox.Text);
            Dispatcher.BeginInvoke((Action)(() =>
            {
                result.ForEach(x => mBooks.Add(x));
            }));
            amountOfBooks = result.Count();
            StatusTextblock.Text = $"Found {amountOfBooks} books by searching for author: \"{AuthorTextbox.Text}\"";
            AuthorTextbox.Text = "Author";
            AuthorTextbox.GotFocus += TextBox_GotFocus;
        }

        public void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= TextBox_GotFocus;
        }

        private void TextBox_LostFocusTitle(object sender, RoutedEventArgs e)
        {
            TextBox box = sender as TextBox;
            if (box.Text.Trim().Equals(string.Empty))
            {
                box.Text = "Title";
                //box.Foreground = Brushes.LightGray;
                box.GotFocus += TextBox_GotFocus;
            }
        }

        private void TextBox_LostFocusAuthor(object sender, RoutedEventArgs e)
        {
            TextBox box = sender as TextBox;
            if (box.Text.Trim().Equals(string.Empty))
            {
                box.Text = "Author";
                //box.Foreground = Brushes.LightGray;
                box.GotFocus += TextBox_GotFocus;
            }
        }

        private void Button_Click_ClearResults(object sender, RoutedEventArgs e)
        {
            StatusTextblock.Text = "";
            mBooks.Clear();
            TitleTextbox.Text = "Title";
            AuthorTextbox.Text = "Author";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        //[NotifyPropertyChangedInvocator]
        //protected void OnPropertyChanged(string propertyName)
        //{
        //    if (PropertyChanged != null)
        //        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //}
    }
}
