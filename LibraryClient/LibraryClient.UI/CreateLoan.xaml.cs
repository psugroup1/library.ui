﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LibraryClient.Integration;
using LibraryClient.Models;

namespace LibraryClient.UI
{
    /// <summary>
    /// Interaction logic for CreateLoan.xaml
    /// </summary>
    public partial class CreateLoan : Page
    {
        private readonly LoanGateway _loanGateway;

        public CreateLoan()
        {
            InitializeComponent();
            _loanGateway = new LoanGateway();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var cardId = Int32.Parse(textBoxCardId.Text);
                var copyId = Int32.Parse(textBoxCopyId.Text);
                var returnLoan = _loanGateway.CreateLoan(cardId, copyId);
                string displayString = "Loan created with ID: " + returnLoan.LoanId.ToString() 
                    + ", cardID: "+ returnLoan.PersonCardID.ToString()
                    + "\nDate created: " + returnLoan.DateCreated.ToString();
                LoanResponse.Text = displayString;
            }
            catch (FormatException fe)
            {
                LoanResponse.Text = "Please dont enter any letters";
            }

            if (_loanGateway.errorMessage != "")
            {
                LoanResponse.Text = "Error message for CardID: " + textBoxCardId.Text
                    + " and CopyID: " + textBoxCopyId.Text + "\n"
                    + _loanGateway.errorMessage.Replace("\"", "");
            }
        }
    }
}
