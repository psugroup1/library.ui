﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using LibraryClient.Models;

namespace LibraryClient.Integration
{
    public class LoanGateway : IloanGateway
    {
        public string errorMessage { get; set; }
        static void Main(string[] args)
        {
        }

        public Loan CreateLoan(int cardId, int copyId)
        {
            var returnLoan = new Loan();
            var client = GetHttpClient();
            var response = client.GetAsync($"Loan/CreateLoan?cardId={cardId}&copyId={copyId}").Result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                returnLoan = response.Content.ReadAsAsync<Loan>().Result;
                errorMessage = "";
            }
            else
            {
                var errorString = response.Content.ReadAsStringAsync();
                errorMessage = errorString.Result;
            }

            return returnLoan;
        }

        public List<Book> SearchBooksTitle(string searchString)
        {
            var books = new List<Book>();
            var client = GetHttpClient();
            var response = client.GetAsync($"Book/SearchBookTitle?searchString={searchString}").Result;
            books = response.Content.ReadAsAsync<List<Book>>().Result;
            return books;
        }

        public List<Book> SearchBooksAuthor(string searchString)
        {
            var books = new List<Book>();
            var client = GetHttpClient();
            var response = client.GetAsync($"Book/SearchBookAuthor?searchString={searchString}").Result;
            books = response.Content.ReadAsAsync<List<Book>>().Result;
            return books;
        }

        private HttpClient GetHttpClient()
        {
            var client = new HttpClient(new HttpClientHandler() { UseDefaultCredentials = true });
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseAdress"]);
            //client.BaseAddress = new Uri("http://localhost:1659/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue($"application/json"));
            return client;
        }

        public int CheckBookAvailability(string ISBN)
        {
            var client = GetHttpClient();
            var response = client.GetAsync($"Book/CheckBookAvailability?isbn={ISBN}").Result;
            return response.Content.ReadAsAsync<int>().Result;
        }
    }
}
